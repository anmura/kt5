import java.util.*;

public class Node {

    private String name;
    private Node firstChild;
    private Node nextSibling;

    Node(String n, Node d, Node r) {
        this.name = n;
        this.firstChild = d;
        this.nextSibling = r;
    }

    public static Node parsePostfix(String s) {

        // Testi osa
        s = s.trim();

        if (s.contains("()")) throw new RuntimeException("Incorrect input: " + s + " (Empty brackets)");
        if (!(s.contains("(") || s.contains(")")) && s.contains(","))
            throw new RuntimeException("Incorrect input: " + s + " (Invalid Sibling)");
        if (!(s.contains("(") || s.contains(")") || s.contains(",")) && s.contains(" "))
            throw new RuntimeException("Incorrect input: " + s + " (Invalid syntax)");
        if (s.contains(",,") || s.contains("(,"))
            throw new RuntimeException("Incorrect input: " + s + " (Invalid syntax)");
        if (s.contains("((") && s.contains("))"))
            throw new RuntimeException("Incorrect input: " + s + " (Invalid syntax)");

        char[] test = s.toCharArray();

        if (String.valueOf(test[0]).equals(")") || String.valueOf(test[test.length - 1]).equals("("))
            throw new RuntimeException("Incorrect input: " + s + " (Invalid syntax)");

        int openBracket = 0;
        int closeBracket = 0;
        int deep = 0;

        StringBuilder nodes = new StringBuilder();
        for (char c : test) {
            String charInStr = String.valueOf(c);

            if (charInStr.equals("(")) {
                openBracket++;
                deep++;
            }
            if (charInStr.equals(")")) {
                closeBracket++;
                deep--;
            }
            if (!",()".contains(charInStr)) {
                nodes.append(c);
            }
            if (deep == 0 && charInStr.equals(",")) {
                throw new RuntimeException("Incorrect input: " + s + " (Invalid Node)");
            }
        }

        if (closeBracket != openBracket) throw new RuntimeException("Incorrect input: " + s + " (Invalid syntax)");

        if (nodes.toString().trim().equals(""))
            throw new RuntimeException("Incorrect input: " + s + " (Invalid syntax)");


        // Node tegemise osa

        s = s.replace(")", "!)!");
        s = s.replace("(", "!(!");
        s = s.replace(",", "!,!");
        s = s.replace("!!", "!");

        char[] c = s.toCharArray();
        if (String.valueOf(c[0]).equals("!")) {
            s = s.substring(1);
        }

        String[] elements = s.split("!");

        return putNode(elements);
    }


    // https://gist.github.com/mxrguspxrt/4113122 (Võtsin rohkem osa putNode-st )
    private static Node putNode(String[] elements) {   // Teeb Node Array-st

        String n = "";
        Node child = null;
        Node sibling = null;

        for (int i = 0; i < elements.length; i++) {
            if (!"(), ".contains(elements[i])) {
                n = elements[i];
            }

            if (elements[i].equals("(")) {

                int deep = 0;
                for (int j = i; j < elements.length; j++) {
                    if (elements[j].equals("(")) {
                        deep++;
                    }
                    if (elements[j].equals(")")) {
                        deep--;
                    }
                    if (deep == 0) {
                        child = putNode(Arrays.copyOfRange(elements, i + 1, j));
                        i = j;
                        break;
                    }
                }
            }

            if (elements[i].equals(",")) {
                sibling = putNode(Arrays.copyOfRange(elements, i + 1, elements.length));
                break;
            }
        }

        return new Node(n, child, sibling);
    }

    private static void putSubNode(StringBuffer s, Node subNode) {
        s.append(subNode.name);

        if (subNode.firstChild != null) {
            s.append("(");
            putSubNode(s, subNode.firstChild);
            s.append(")");
        }
        if (subNode.nextSibling != null) {
            s.append(",");
            putSubNode(s, subNode.nextSibling);

        }
    }

    public String leftParentheticRepresentation() {

        StringBuffer res = new StringBuffer();
        putSubNode(res, this);

        return res.toString();
    }

    public String xmlRepresentation() {
        StringBuffer res = new StringBuffer();
        putSubxmlNode(res, this, 1);

        return res.toString();
    }

    private static void putSubxmlNode(StringBuffer res, Node subNode, int deep) {
        res.append("\t".repeat(deep - 1));
        res.append(String.format("<L%s> %s", deep, subNode.name));

        if (subNode.firstChild != null) {
            res.append("\n");
            deep++;
            putSubxmlNode(res, subNode.firstChild, deep);
            deep--;
            res.append("\t".repeat(deep - 1));
            res.append(String.format("</L%s>\n", deep));
        } else if (subNode.firstChild == null) {
            res.append(String.format(" </L%s>\n", deep));
        }

        if (subNode.nextSibling != null) {
            putSubxmlNode(res, subNode.nextSibling, deep);
        }
    }

    public static void main(String[] param) {

        Node n = Node.parsePostfix("A");
        String xml2 = n.xmlRepresentation();
        System.out.println(xml2);


    }
}

